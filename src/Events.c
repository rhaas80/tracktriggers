#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

void Calc_Separation(CCTK_ARGUMENTS);

void Initialization(CCTK_ARGUMENTS)
{
   DECLARE_CCTK_ARGUMENTS;

   *hc_sep_12=1000000000;
   *merger_flag=0;
   *merger_time=10000000000;
   CCTK_INFO("Initialising variables");
   
   return;
}

void Check_For_Events(CCTK_ARGUMENTS)
{
   DECLARE_CCTK_ARGUMENTS;
   DECLARE_CCTK_PARAMETERS;
   char parstring[200], timestring[200];

   if (check_every == 0)
     return;
   
   // Things to trigger
   // 1 - Stop searching for individual horizons
   // 2 - Start searching common
   // 3 - Reduce number of reflevel
   // 4 - Increase AHF output
   // 5 - Trigger termination after delay
   // 6 - Broadcast merger
   if ( sf_active[surface1_index] != 1 )
   {
     sprintf(parstring, "Surface %d is not active. Doing nothing.", surface1_index);
     CCTK_INFO(parstring);
   }
   else
   {
     if ( sf_active[surface2_index] != 1 )
     {
       sprintf(parstring, "Surface %d is not active. Doing nothing.", surface2_index);
       CCTK_INFO(parstring);
     }
     else
     {
       if ( (*merger_flag==0) && (cctk_iteration % check_every == 0) )
       {
         Calc_Separation(CCTK_PASS_CTOC);
         sprintf(parstring, "The separation between surface %d and surface %d is %f", surface1_index, surface2_index, *hc_sep_12);
         CCTK_INFO(parstring);
         
         if ( *hc_sep_12 <= min_separation )
         {
	   *merger_flag = 1;

           sprintf(parstring, "Surface %d and surface %d are closer than %f at iteration %d, time %f",
                   surface1_index, surface2_index, min_separation, cctk_iteration, cctk_time);
           CCTK_INFO(parstring);
       
           if (stop_searching_1)
           {
             sprintf(parstring, "disable_horizon[%d]", surface1_index+1);
             CCTK_ParameterSet(parstring, "AHFinderDirect", "true");
           }
           if (stop_searching_2)
           {
             sprintf(parstring, "disable_horizon[%d]", surface2_index+1);
             CCTK_ParameterSet(parstring, "AHFinderDirect", "true");
           }
           if (start_searching_1)
           {
             sprintf(parstring, "disable_horizon[%d]", surface3_index+1);
             CCTK_ParameterSet(parstring, "AHFinderDirect", "false");
             sprintf(parstring, "find_after_individual[%d]", surface3_index+1);
             sprintf(timestring, "%f", cctk_time);
             CCTK_ParameterSet(parstring, "AHFinderDirect", timestring);
           }

           if (delete_finest_reflevel)
           {
             printf("Deleting finest reflevel...\n");
             num_levels[0] -= 1;
             num_levels[1] -= 1;
           }

           sprintf(parstring, "%d", increase_AHF_output);
           if (increase_AHF_output) CCTK_ParameterSet("output_h_every", "AHFinderDirect", parstring);
           if (trigger_termination_after_delay)
           {
             CCTK_VInfo(CCTK_THORNSTRING,"Will trigger termination at t=%f; delayed %f after surfaces %d and %d became closer than %f",
                        cctk_time+trigger_termination_after_delay, trigger_termination_after_delay, surface1_index, surface2_index, min_separation);
             *merger_time = cctk_time;
           }

           //if (broadcast_merger)
         }
       }
     }
   }

   if (cctk_time >= *merger_time + trigger_termination_after_delay)
   {
     CCTK_VInfo(CCTK_THORNSTRING,"Triggering termination at t=%f; delayed %f after surfaces %d and %d became closer than %f",
                cctk_time, trigger_termination_after_delay, surface1_index, surface2_index, min_separation);
     CCTK_TerminateNext (cctkGH);
   }
    
   return;
}

void Calc_Separation(CCTK_ARGUMENTS)
{
   DECLARE_CCTK_ARGUMENTS
   DECLARE_CCTK_PARAMETERS

   CCTK_REAL xs1, ys1, zs1, xs2, ys2, zs2;
   char parstring[200];

   xs1 = sf_centroid_x[surface1_index];
   ys1 = sf_centroid_y[surface1_index];
   zs1 = sf_centroid_z[surface1_index];
   xs2 = sf_centroid_x[surface2_index];
   ys2 = sf_centroid_y[surface2_index];
   zs2 = sf_centroid_z[surface2_index];

   *hc_sep_12 = sqrt(pow(xs1-xs2,2)+pow(ys1-ys2,2)+pow(zs1-zs2,2));

   return;
}
